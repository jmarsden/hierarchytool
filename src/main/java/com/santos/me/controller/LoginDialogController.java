/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me.controller;

import com.santos.me.IntentHub;
import com.santos.me.database.HibernateUtil;
import com.santos.me.intent.ResetConnectionIntent;
import java.util.Properties;

/**
 *
 * @author jmarsden
 */
public class LoginDialogController extends AppController {

    public void handleLogin(Properties properties) {

        HibernateUtil.connect(properties);

        HibernateUtil.checkDatabase();

        IntentHub.lookupSingleton().notifyIntent(new ResetConnectionIntent(this));
    }
}
