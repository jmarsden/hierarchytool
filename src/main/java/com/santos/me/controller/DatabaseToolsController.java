/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me.controller;

/**
 *
 * @author jmarsden
 */
public class DatabaseToolsController extends AppController {

    boolean busy;
    StringBuffer buffer;
    
    public DatabaseToolsController() {
        busy = false;
        buffer = new StringBuffer();
    }
    
    public void createDatabase(String externalDriver, String externalConnection, String externalUsername, String externalPassword, String externalPrefix, String localDriver, String localConnection, String localUsername, String localPassword, String localPrefix) {
        busy = true;
        System.out.println("Create!");
    
        busy = false;
    }
}
