/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me.controller;

import com.santos.me.database.HibernateUtil;
import com.santos.me.IntentHub;
import com.santos.me.intent.*;
import com.santos.me.model.ExternalViewModel;
import com.santos.me.objects.ExternalLocation;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author jmarsden
 */
public class ExternalViewController extends AppController {

    private ExternalViewModel model;
    private int counter;
    private Iterator<ExternalLocation> resultSet;
    private int levelsToLoadInDetails;
    private ExternalLocation selectedLocation;
    private boolean expand;

    public ExternalViewController(ExternalViewModel model) {
        this.model = model;
        this.levelsToLoadInDetails = 2;
        this.selectedLocation = null;
        this.expand = true;
    }

    public ExternalViewModel getModel() {
        return model;
    }

    public void setModel(ExternalViewModel model) {
        this.model = model;
    }

    public boolean isExpand() {
        return expand;
    }

    public void setExpand(boolean expand) {
        this.expand = expand;
    }

    public int getLevelsToLoadInDetails() {
        return levelsToLoadInDetails;
    }

    public void setLevelsToLoadInDetails(int levelsToLoadInDetails) {
        this.levelsToLoadInDetails = levelsToLoadInDetails;
    }

    public ExternalLocation getSelectedLocation() {
        return selectedLocation;
    }

    public void setSelectedLocation(ExternalLocation selectedLocation) {
        this.selectedLocation = selectedLocation;
    }

    public List<ExternalLocation> getExternalLocationRoots() {
        return model.getExternalLocationRoots();
    }

    public boolean externalLocationHasChildren(ExternalLocation parent) {
        return !parent.getChildren().isEmpty();
    }

    public List<ExternalLocation> getExternalLocationChildren(ExternalLocation parent) {
        return parent.getChildren();
    }

    public void filterChanged() {
        if (resultSet != null) {
            resultSet = null;
        }
        IntentHub.lookupSingleton().notifyIntent(new ExternalViewFilterChangedIntent(this));
    }

    public void filterToIdentifer(String identifierQueryFilter, String descriptionQueryFilter) {
        if (resultSet != null) {
            // Scroll
            if (resultSet.hasNext()) {
                ExternalLocation result = resultSet.next();
                counter++;
                //System.out.println("Counter:" + counter);
                selectIdentifier(result);
            } else {
                resultSet = null;
                filterToIdentifer(identifierQueryFilter, descriptionQueryFilter);
            }
        } else {


            Session session = HibernateUtil.getSessionFactory().openSession();
            Criteria critera = session.createCriteria(ExternalLocation.class);

            String workingTagFilter = "";
            String workingDescriptionFilter = "";

            if (identifierQueryFilter != null && !identifierQueryFilter.trim().equals("")) {
                if (identifierQueryFilter.charAt(0) != '=') {
                    if (identifierQueryFilter.contains("%")) {
                        workingTagFilter = identifierQueryFilter;
                    } else {
                        workingTagFilter = "%" + identifierQueryFilter + "%";
                    }
                    critera.add(Restrictions.like("identifier", workingTagFilter).ignoreCase());
                } else {
                    workingTagFilter = identifierQueryFilter.subSequence(1, identifierQueryFilter.length()).toString();
                    critera.add(Restrictions.eq("identifier", workingTagFilter));
                }
            }

            if (descriptionQueryFilter != null && !descriptionQueryFilter.trim().equals("")) {
                if (descriptionQueryFilter.charAt(0) != '=') {
                    if (descriptionQueryFilter.contains("%")) {
                        workingDescriptionFilter = descriptionQueryFilter;
                    } else {
                        workingDescriptionFilter = "%" + descriptionQueryFilter + "%";
                    }
                    critera.add(Restrictions.like("description", workingDescriptionFilter).ignoreCase());
                } else {
                    workingDescriptionFilter = descriptionQueryFilter.subSequence(1, descriptionQueryFilter.length()).toString();
                    critera.add(Restrictions.eq("description", workingDescriptionFilter));
                }
            }
            resultSet = critera.list().iterator();
            if (resultSet == null) {
                return;
            }

            if (resultSet.hasNext()) {
                counter = 0;
                ExternalLocation result = resultSet.next();
                selectIdentifier(result);
            } else {
                IntentHub.lookupSingleton().notifyIntent(new ExternalViewFilterNotFoundIntent(this));
            }
        }
    }

    private void selectIdentifier(ExternalLocation result) {
        IntentHub.lookupSingleton().notifyIntent(new ExternalViewLocationFocusIntent(this, result));
    }

    public void triggerBusy() {
        IntentHub.lookupSingleton().notifyIntent(new ExternalViewBusyIntent(this, true));
    }

    public void triggerNotBusy() {
        IntentHub.lookupSingleton().notifyIntent(new ExternalViewBusyIntent(this, false));
    }

    public void setNumberLevelsToLoad(int levels) {
        levelsToLoadInDetails = levels;
        if (selectedLocation != null) {
            IntentHub.lookupSingleton().notifyIntent(new ExternalViewRefeshTablesIntent(this, selectedLocation, levelsToLoadInDetails));
        }
    }

    public void selectLocation(ExternalLocation externalLocation) {
        selectedLocation = externalLocation;
        IntentHub.lookupSingleton().notifyIntent(new ExternalViewLocationSelectIntent(this, externalLocation, levelsToLoadInDetails));
    }

    public void reset() {
        model.reset();
        resultSet = null;
        selectedLocation = null;
        expand = true;
    }

    public void loadLocationDetails(List<ExternalLocation> externalLocations) {
        IntentHub.lookupSingleton().notifyIntent(new ExternalViewLocationDetailsIntent(this, externalLocations));
    }

    public boolean levelCountCheck() {
        if (levelsToLoadInDetails > 6) {
            return true;
        } else {
            return false;
        }
    }
}
