/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me.view;

import com.santos.me.Intent;
import com.santos.me.IntentHub;
import com.santos.me.IntentReceiver;
import com.santos.me.controller.ExternalViewController;
//import com.santos.me.gui.NiceBorder;
import com.santos.me.intent.*;
import com.santos.me.objects.ExternalAsset;
import com.santos.me.objects.ExternalLocation;
import com.santos.me.objects.ExternalSchedule;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.jdesktop.swingx.*;
import org.jdesktop.swingx.painter.BusyPainter;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;

/**
 *
 * @author jmarsden
 */
public class ExternalViewPanel extends JXPanel {

    private JSplitPane splitPane;
    private ExternalViewController controller;

    public ExternalViewPanel(ExternalViewController controller) {
        this.controller = controller;
    }

    @Override
    public void addNotify() {
        super.addNotify();

        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);

        setLayout(new BorderLayout());

        splitPane.setDividerLocation(200);
        splitPane.setOneTouchExpandable(true);
        splitPane.setContinuousLayout(true);

        JPanel splitPanel = new JPanel();
        splitPanel.setLayout(new BorderLayout());
        splitPanel.add(new ExternalTreeViewPanel(), BorderLayout.CENTER);
        splitPanel.setBackground(Color.WHITE);
        Image logo = null;
        try {
            URL logoUrl = Object.class.getResource("/santos.png");

            Toolkit kit = Toolkit.getDefaultToolkit();
            logo = kit.getImage(logoUrl);
        } catch (Exception ex) {
            Logger.getLogger(ExternalViewPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (logo != null) {
            JLabel imageLabel = new JLabel(new ImageIcon(logo));
            imageLabel.setBackground(Color.WHITE);
            //imageLabel.setBorder(new NiceBorder(Color.GRAY, new Insets(4, 5, 4, 5), false, false, true, true));
            splitPanel.add(imageLabel, BorderLayout.SOUTH);
        }

        splitPane.setLeftComponent(splitPanel);
        splitPane.setRightComponent(new ExternalTablesViewPanel());

        add(splitPane, BorderLayout.CENTER);
    }

    public class ExternalTreeViewPanel extends JXPanel implements IntentReceiver {

        private JScrollPane treePane;
        private JXTree tree;
        private ExternalTreeViewModel treeModel;
        private JXPanel searchPanel;
        private JXTextField identifierQuery;
        private JXTextField descriptionQuery;
        private JXBusyLabel busyLabel;
        private final Component component = this;

        @Override
        public void addNotify() {
            super.addNotify();
            TreeNode rootNode = new DefaultMutableTreeNode("Hierarchy");
            treeModel = new ExternalTreeViewModel(rootNode);
            tree = new JXTree(treeModel);
            tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
            tree.setRootVisible(false);
            tree.setBorder(new EmptyBorder(4, 0, 4, 0));

            MouseAdapter mouseAdapter = new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    if (e.isPopupTrigger()) {
                        myPopupEvent(e);
                    }
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    if (e.isPopupTrigger()) {
                        myPopupEvent(e);
                    }
                }

                private void myPopupEvent(MouseEvent e) {
                    int x = e.getX();
                    int y = e.getY();
                    JTree tree = (JTree) e.getSource();
                    TreePath path = tree.getPathForLocation(x, y);
                    if (path == null) {
                        return;
                    }
                    tree.setSelectionPath(path);

                    DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) path.getLastPathComponent();
                    String identifier = null;
                    if (treeNode.getUserObject() instanceof ExternalLocation) {
                        ExternalLocation location = (ExternalLocation) treeNode.getUserObject();
                        identifier = location.getIdentifier();
                    } else {
                        identifier = treeNode.toString();
                    }

                    String label = String.format("Add %s to Working Set", identifier);
                    JPopupMenu popup = new JPopupMenu();
                    popup.add(new JMenuItem(label));
                    popup.show(tree, x, y);
                }
            };
            tree.addMouseListener(mouseAdapter);

            tree.addTreeSelectionListener(new TreeSelectionListener() {
                @Override
                public void valueChanged(TreeSelectionEvent e) {
                    if (e.getNewLeadSelectionPath() != null) {
                        if (controller.levelCountCheck()) {
                            int n = JOptionPane.showConfirmDialog(
                                    component,
                                    "You have a lot of levels selected to load. Are you sure you want to load them all?",
                                    "Lots of levels",
                                    JOptionPane.YES_NO_OPTION);
                            if (n == 1) {
                                IntentHub.lookupSingleton().notifyIntent(new ExternalViewSelectLevelsToLoadIntent(controller, 2));
                            }
                        }
                    }
                    controller.selectLocation((ExternalLocation) ((DefaultMutableTreeNode) e.getPath().getLastPathComponent()).getUserObject());
                }
            });

            treePane = new JScrollPane(tree);
            treePane.setBackground(Color.WHITE);
            //treePane.setBorder(new NiceBorder(Color.GRAY, new Insets(4, 5, 4, 5), true, false, true, true));

            searchPanel = new JXPanel();
            searchPanel.setLayout(new BorderLayout());
            searchPanel.setBackground(Color.WHITE);


            identifierQuery = new JXTextField("Identifier");
            identifierQuery.setColumns(8);
            identifierQuery.setToolTipText("Identfier Query");
            //identifierQuery.setBorder(new NiceBorder(Color.GRAY, new Insets(5, 5, 5, 5), false, false, false, true));
            descriptionQuery = new JXTextField("Description");
            descriptionQuery.setToolTipText("Description Query");
            //descriptionQuery.setBorder(new NiceBorder(Color.GRAY, new Insets(5, 5, 5, 5), false, false, false, true));

            identifierQuery.setBackground(Color.WHITE);
            descriptionQuery.setBackground(Color.WHITE);

            identifierQuery.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent e) {
//                    if (controller.levelCountCheck()) {
//                        int n = JOptionPane.showConfirmDialog(
//                                component,
//                                "You have a lot of levels selected to load. Are you sure you want to load them all?",
//                                "Lots of levels",
//                                JOptionPane.YES_NO_OPTION);
//                        if (n == 1) {
//                            IntentHub.lookupSingleton().notifyIntent(new ExternalViewSelectLevelsToLoadIntent(controller, 2));
//                        }
//                    }
                    controller.triggerBusy();
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            controller.filterToIdentifer(identifierQuery.getText(), descriptionQuery.getText());
                        }
                    });
                    controller.triggerNotBusy();
                }
            });

            identifierQuery.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    controller.filterChanged();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    controller.filterChanged();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                }
            });

            descriptionQuery.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent e) {
//                    if (controller.levelCountCheck()) {
//                        int n = JOptionPane.showConfirmDialog(
//                                component,
//                                "You have a lot of levels selected to load. Are you sure you want to load them all?",
//                                "Lots of levels",
//                                JOptionPane.YES_NO_OPTION);
//                        if (n == 1) {
//                            IntentHub.lookupSingleton().notifyIntent(new ExternalViewSelectLevelsToLoadIntent(controller, 2));
//                        }
//                    }
                    controller.triggerBusy();
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            controller.filterToIdentifer(identifierQuery.getText(), descriptionQuery.getText());
                        }
                    });
                    controller.triggerNotBusy();
                }
            });

            descriptionQuery.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    controller.filterChanged();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    controller.filterChanged();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                }
            });

            BusyPainter painter = new BusyPainter();
            painter.setTrailLength(4);
            painter.setPoints(8);

            painter.setBaseColor(Color.LIGHT_GRAY);
            painter.setHighlightColor(Color.RED);

            busyLabel = new JXBusyLabel();
            busyLabel.setBusyPainter(painter);
            //busyLabel.setBorder(new NiceBorder(Color.GRAY, new Insets(4, 5, 4, 5), false, true, false, true));
            //busyLabel.setBusy(true);

            searchPanel.add(identifierQuery, BorderLayout.LINE_START);
            searchPanel.add(descriptionQuery, BorderLayout.CENTER);
            //searchPanel.add(busyLabel, BorderLayout.LINE_END);

            JXPanel treeViewTreePanel = new JXPanel();
            treeViewTreePanel.setLayout(new BorderLayout());
            treeViewTreePanel.add(searchPanel, BorderLayout.NORTH);
            treeViewTreePanel.add(treePane, BorderLayout.CENTER);

            setLayout(new BorderLayout());

            add(treeViewTreePanel, BorderLayout.CENTER);

            IntentHub.lookupSingleton().registerForIntent(this, ResetConnectionIntent.class);
            IntentHub.lookupSingleton().registerForIntent(this, ExternalViewFilterNotFoundIntent.class);
            IntentHub.lookupSingleton().registerForIntent(this, ExternalViewFilterChangedIntent.class);
            IntentHub.lookupSingleton().registerForIntent(this, ExternalViewLocationFocusIntent.class);
            IntentHub.lookupSingleton().registerForIntent(this, ExternalViewBusyIntent.class);

        }

        public void receiveIntent(Intent intent) {
            if (intent.getClass().equals(ResetConnectionIntent.class)) {
                controller.reset();
                treeModel.refresh();
                treeModel.reload();
            } else if (intent.getClass().equals(ExternalViewLocationFocusIntent.class)) {

                TreeSelectionModel selectionModel = tree.getSelectionModel();
                selectionModel.clearSelection();

                tree.collapseAll();

                treeModel.reset();
                treeModel.reload();

                System.gc();

                ArrayList<Object> treePathArray = new ArrayList<Object>();

                ExternalLocation currentLocation = ((ExternalViewLocationFocusIntent) intent).getLocation();
                do {
                    treePathArray.add(treeModel.getNodeForExternalLocation(currentLocation));
                } while ((currentLocation = currentLocation.getParent()) != null);

                treePathArray.add(tree.getModel().getRoot());

                Object[] treePathObjects = new Object[treePathArray.size()];
                for (int i = 0; i < treePathArray.size(); i++) {
                    treePathObjects[i] = treePathArray.get(treePathArray.size() - i - 1);
                }

                TreePath treePath = new TreePath(treePathObjects);
                selectionModel.setSelectionPath(treePath);


                //tree.expandPath(treePath);
                //tree.setSelectionPath(treePath);
                tree.scrollPathToVisible(treePath);

            } else if (intent.getClass().equals(ExternalViewFilterNotFoundIntent.class)) {
                searchPanel.setBackground(Color.RED);
                identifierQuery.setBackground(Color.RED);
                descriptionQuery.setBackground(Color.RED);
            } else if (intent.getClass().equals(ExternalViewFilterChangedIntent.class)) {
                searchPanel.setBackground(Color.WHITE);
                identifierQuery.setBackground(Color.WHITE);
                descriptionQuery.setBackground(Color.WHITE);
            } else if (intent.getClass().equals(ExternalViewBusyIntent.class)) {
                ExternalViewBusyIntent externalVuewBusyIntent = (ExternalViewBusyIntent) intent;
                busyLabel.setBusy(externalVuewBusyIntent.isBusy());
            }
        }
    }

    public class ExternalTablesViewPanel extends JXPanel implements IntentReceiver {

        private JSplitPane splitView;
        private JXTreeTable locationTreeTable;
        private ExternalLocationTreeTableModel locationTreeTableModel;
        private JXTreeTable assetTreeTable;
        private ExternalAssetTreeTableModel assetTreeTableModel;
        private JXTreeTable scheduleTreeTable;
        private ExternalScheduleTreeTableModel scheduleTreeTableModel;
        private JComboBox levelsToLoad;

        @Override
        public void addNotify() {
            super.addNotify();

            setLayout(new BorderLayout());

            JPanel optionPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 0));
            optionPanel.setBackground(Color.WHITE);
            //optionPanel.setBorder(new NiceBorder(Color.GRAY, new Insets(1, 5, 0, 5), false, true, false, false));

            JLabel levelsToLoadLabel = new JLabel("Levels to Load:");

            final String[] levelsToLoadValues = {"1", "2", "3", "4", "5", "6", "ALL"};
            levelsToLoad = new JComboBox(levelsToLoadValues);
            levelsToLoad.setSelectedIndex(1);

            levelsToLoad.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String value = levelsToLoad.getSelectedItem().toString();
                    if (value.equals("ALL")) {
                        controller.setNumberLevelsToLoad(50);
                    } else {
                        controller.setNumberLevelsToLoad(Integer.parseInt(value));
                    }
                }
            });

            optionPanel.add(levelsToLoadLabel);
            optionPanel.add(levelsToLoad);

            JButton selectAllButton = new JButton("Select All");
            selectAllButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    selectAllInLocationView();
                }
            });
            JButton collapseButton = new JButton("Collapse All");
            collapseButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    collapseAllInLocationView();
                }
            });
            JButton expandButton = new JButton("Expand All");
            expandButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    expandAllInLocationView();
                }
            });
            JButton reportButton = new JButton("Run Report");
            reportButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        runReport();
                    } catch (JRException ex) {
                        Logger.getLogger(ExternalViewPanel.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(ExternalViewPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });

            optionPanel.add(selectAllButton);
            optionPanel.add(collapseButton);
            optionPanel.add(expandButton);
            //optionPanel.add(reportButton);

            add(optionPanel, BorderLayout.NORTH);
            splitView = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
            add(splitView, BorderLayout.CENTER);

            locationTreeTableModel = new ExternalLocationTreeTableModel();
            locationTreeTable = new JXTreeTable(locationTreeTableModel);
            locationTreeTable.setRootVisible(false);
            setExternalLocationTableAppearance(locationTreeTable);

            locationTreeTable.addTreeSelectionListener(new TreeSelectionListener() {
                @Override
                public void valueChanged(TreeSelectionEvent e) {
                    TreePath[] treePaths = locationTreeTable.getTreeSelectionModel().getSelectionPaths();
                    List<ExternalLocation> locations = new ArrayList<ExternalLocation>();
                    for (int i = 0; i < treePaths.length; i++) {
                        TreePath treePath = treePaths[i];
                        DefaultMutableTreeTableNode locationNode = (DefaultMutableTreeTableNode) treePath.getLastPathComponent();
                        ExternalLocation location = (ExternalLocation) locationNode.getUserObject();
                        locations.add(location);

                    }
                    controller.loadLocationDetails(locations);
                }
            });

            JScrollPane locationTableScrollPane = new JScrollPane(locationTreeTable);

            splitView.setTopComponent(locationTableScrollPane);
            locationTreeTable.updateUI();

            JTabbedPane detailsTabPane = new JTabbedPane();

            assetTreeTableModel = new ExternalAssetTreeTableModel();
            assetTreeTable = new JXTreeTable(assetTreeTableModel);
            setExternalAssetTableAppearance(assetTreeTable);

            JScrollPane assetScrollPane = new JScrollPane(assetTreeTable);

            detailsTabPane.add(assetScrollPane, "Assets");

            scheduleTreeTableModel = new ExternalScheduleTreeTableModel();
            scheduleTreeTable = new JXTreeTable(scheduleTreeTableModel);
            setExternalScheduleTableAppearance(scheduleTreeTable);

            JScrollPane scheduleScrollPane = new JScrollPane(scheduleTreeTable);

            detailsTabPane.add(scheduleScrollPane, "Schedules");

            splitView.setBottomComponent(detailsTabPane);

            assetTreeTable.updateUI();

            splitView.setDividerLocation(300);

            IntentHub.lookupSingleton().registerForIntent(this, ExternalViewLocationSelectIntent.class);
            IntentHub.lookupSingleton().registerForIntent(this, ExternalViewRefeshTablesIntent.class);
            IntentHub.lookupSingleton().registerForIntent(this, ExternalViewLocationDetailsIntent.class);
            IntentHub.lookupSingleton().registerForIntent(this, ExternalViewSelectLevelsToLoadIntent.class);
        }

        public void receiveIntent(Intent intent) {
            if (intent.getClass().equals(ExternalViewLocationSelectIntent.class)) {
                ExternalViewLocationSelectIntent externalLocationSelection = (ExternalViewLocationSelectIntent) intent;
                selectLocation(externalLocationSelection.getLocation(), externalLocationSelection.getLevels());
            } else if (intent.getClass().equals(ExternalViewRefeshTablesIntent.class)) {
                ExternalViewRefeshTablesIntent externalLocationSelection = (ExternalViewRefeshTablesIntent) intent;
                selectLocation(externalLocationSelection.getLocation(), externalLocationSelection.getLevels());
            } else if (intent.getClass().equals(ExternalViewLocationDetailsIntent.class)) {
                ExternalViewLocationDetailsIntent externalViewLocationDetails = (ExternalViewLocationDetailsIntent) intent;
                loadLocationListDetails(externalViewLocationDetails.getLocations());
            } else if (intent.getClass().equals(ExternalViewSelectLevelsToLoadIntent.class)) {
                ExternalViewSelectLevelsToLoadIntent externalViewSelectLevelsToLoadIntent = (ExternalViewSelectLevelsToLoadIntent) intent;
                setLevelsToLoad(externalViewSelectLevelsToLoadIntent.getLevels());
            }
        }

        private void selectLocation(ExternalLocation currentLocation, int levelsToLoad) {
            locationTreeTableModel.reset();

            List<ExternalLocation> locationList = new ArrayList<ExternalLocation>();

            DefaultMutableTreeTableNode rootNode = (DefaultMutableTreeTableNode) locationTreeTableModel.getRoot();

            ExternalLocationTreeTableNode baseNode = locationTreeTableModel.getExternalLocationTreeTableNode(currentLocation);

            rootNode.add(baseNode);
            addExternalLocationLevelToTreeModel(currentLocation, baseNode, 0, levelsToLoad, locationList);

            locationTreeTable.updateUI();
            if (controller.isExpand()) {
                locationTreeTable.expandAll();
            }
            if (locationList.size() <= 20) {
                locationTreeTable.selectAll();
            } else {
                locationTreeTable.clearSelection();
            }
        }

        public void expandAllInLocationView() {
            locationTreeTable.expandAll();
        }

        public void collapseAllInLocationView() {
            locationTreeTable.collapseAll();
            locationTreeTable.expandPath(new TreePath(locationTreeTableModel.getRoot()));
        }

        private void selectAllInLocationView() {
            locationTreeTable.selectAll();
        }

        private void runReport() throws JRException, IOException {
            String fileName = "reports/BasicReport.xml";
            URL url = null;
            File file = new File(fileName);
            if (file.exists()) {
                url = file.toURI().toURL();
            } else {
                url = Object.class.getResource("/" + fileName);
            }

            Map parameters = new HashMap();
            parameters.put("Title", "Test Report");

            List<ExternalLocation> locationList = new ArrayList<ExternalLocation>();

            DefaultMutableTreeTableNode node = (DefaultMutableTreeTableNode) locationTreeTableModel.getRoot();
            Enumeration<ExternalLocationTreeTableNode> nodeChildren = (Enumeration<ExternalLocationTreeTableNode>) node.children();
            while (nodeChildren.hasMoreElements()) {
                ExternalLocationTreeTableNode childNode = nodeChildren.nextElement();
                ExternalLocation child = (ExternalLocation) childNode.getUserObject();
                locationList.add(child);
            }

            JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(locationList);

            JasperDesign jasperDesign = JRXmlLoader.load(url.openStream());
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);

            //JasperExportManager.exportReportToPdfFile(jasperPrint, "C:/Scratch/test_jasper.pdf"); 


            JasperViewer.viewReport(jasperPrint);
        }

        private void addExternalLocationLevelToTreeModel(ExternalLocation location, ExternalLocationTreeTableNode locationNode, int level, int maxLevels, List<ExternalLocation> locationList) {
            List<ExternalLocation> childrenLocationList = location.getChildren();
            for (int i = 0; i < childrenLocationList.size(); i++) {
                ExternalLocation loc = childrenLocationList.get(i);
                locationList.add(loc);
                ExternalLocationTreeTableNode currentNode = locationTreeTableModel.getExternalLocationTreeTableNode(loc);

                locationNode.add(currentNode);
                if (level + 1 < maxLevels) {
                    addExternalLocationLevelToTreeModel(childrenLocationList.get(i), currentNode, level + 1, maxLevels, locationList);
                }
            }
        }

        private void setExternalLocationTableAppearance(JXTreeTable locationTable) {
            TableColumnModel locationTableColumnModel = locationTable.getColumnModel();
            TableColumn column = locationTableColumnModel.getColumn(0);
            column.setPreferredWidth(200);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(1);
            column.setMinWidth(30);
            column.setPreferredWidth(30);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(2);
            column.setMinWidth(100);
            column.setPreferredWidth(200);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(3);
            column.setPreferredWidth(65);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(4);
            column.setPreferredWidth(50);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(5);
            column.setPreferredWidth(50);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(6);
            column.setPreferredWidth(50);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(7);
            column.setPreferredWidth(80);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(8);
            column.setPreferredWidth(85);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(9);
            column.setPreferredWidth(85);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(10);
            column.setPreferredWidth(50);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(11);
            column.setPreferredWidth(80);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(12);
            column.setPreferredWidth(50);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(13);
            column.setPreferredWidth(50);
            column.setResizable(true);
        }

        private void setExternalAssetTableAppearance(JXTreeTable locationTable) {
            TableColumnModel locationTableColumnModel = locationTable.getColumnModel();
            TableColumn column = locationTableColumnModel.getColumn(0);
            column.setPreferredWidth(100);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(1);
            column.setMinWidth(50);
            column.setPreferredWidth(100);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(2);
            column.setPreferredWidth(200);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(3);
            column.setPreferredWidth(50);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(4);
            column.setPreferredWidth(50);
            column.setResizable(true);
        }

        private void setExternalScheduleTableAppearance(JXTreeTable locationTable) {
            TableColumnModel locationTableColumnModel = locationTable.getColumnModel();
            TableColumn column = locationTableColumnModel.getColumn(0);
            column.setPreferredWidth(80);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(1);
            column.setMinWidth(50);
            column.setPreferredWidth(300);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(2);
            column.setPreferredWidth(80);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(3);
            column.setPreferredWidth(80);
            column.setResizable(true);

            column = locationTableColumnModel.getColumn(4);
            column.setPreferredWidth(50);
            column.setResizable(true);
        }

        private void loadLocationListDetails(List<ExternalLocation> locationList) {
            assetTreeTableModel.reset();
            scheduleTreeTableModel.reset();
            int counter = 0;

            for (ExternalLocation externalLocation : locationList) {
                List<ExternalAsset> externalAssetList = externalLocation.getAssets();
                for (ExternalAsset externalAsset : externalAssetList) {
                    DefaultMutableTreeTableNode rootNode = (DefaultMutableTreeTableNode) assetTreeTableModel.getRoot();
                    ExternalAssetTreeTableNode baseNode = assetTreeTableModel.getExternalAssetTreeTableNode(externalAsset);
                    rootNode.add(baseNode);
                }

                List<ExternalSchedule> externalScheduleList = externalLocation.getSchedules();
                for (ExternalSchedule externalSchedule : externalScheduleList) {
                    DefaultMutableTreeTableNode rootNode = (DefaultMutableTreeTableNode) scheduleTreeTableModel.getRoot();
                    ExternalScheduleTreeTableNode baseNode = scheduleTreeTableModel.getExternalScheduleTreeTableNode(externalSchedule);
                    rootNode.add(baseNode);
                }

                counter++;
            }
            assetTreeTable.updateUI();
            assetTreeTable.expandAll();
            scheduleTreeTable.updateUI();
            scheduleTreeTable.expandAll();
        }

        private void setLevelsToLoad(int levels) {
            if (levels < 10) {
                levelsToLoad.setSelectedItem("" + levels);
            } else {
                levelsToLoad.setSelectedItem("ALL");
            }

        }
    }

    public class ExternalTreeViewModel extends DefaultTreeModel {

        List<ExternalLocation> roots;
        Map<ExternalLocation, TreeNode> locationTreeNodes;

        public ExternalTreeViewModel(TreeNode root, boolean asksAllowsChildren) {
            super(root, asksAllowsChildren);
            locationTreeNodes = new HashMap<ExternalLocation, TreeNode>();
        }

        public ExternalTreeViewModel(TreeNode root) {
            super(root);
            locationTreeNodes = new HashMap<ExternalLocation, TreeNode>();
        }

        public void reset() {
            locationTreeNodes = new HashMap<ExternalLocation, TreeNode>();
        }

        public void refresh() {
            roots = controller.getExternalLocationRoots();
        }

        @Override
        public Object getChild(Object parent, int index) {
            if (parent.equals(this.root)) {
                return getNodeForExternalLocation(roots.get(index));
            } else {
                DefaultMutableTreeNode parentTreeNode = (DefaultMutableTreeNode) parent;
                if (parentTreeNode.getUserObject() instanceof ExternalLocation) {
                    ExternalLocation externalLocation = (ExternalLocation) parentTreeNode.getUserObject();
                    return getNodeForExternalLocation(externalLocation.getChildren().get(index));
                }
            }
            return super.getChild(parent, index);
        }

        @Override
        public int getChildCount(Object parent) {
            if (roots == null) {
                return 0;
            }

            if (parent.equals(this.root)) {
                return roots.size();
            } else {
                DefaultMutableTreeNode parentTreeNode = (DefaultMutableTreeNode) parent;
                if (parentTreeNode.getUserObject() instanceof ExternalLocation) {
                    ExternalLocation externalLocation = (ExternalLocation) parentTreeNode.getUserObject();
                    return externalLocation.getChildren().size();
                }
            }
            return super.getChildCount(parent);
        }

        @Override
        public int getIndexOfChild(Object parent, Object child) {
            if (parent.equals(this.root)) {
                DefaultMutableTreeNode childTreeNode = (DefaultMutableTreeNode) child;
                return roots.indexOf(childTreeNode.getUserObject());
            } else {
                DefaultMutableTreeNode parentTreeNode = (DefaultMutableTreeNode) parent;
                if (parentTreeNode.getUserObject() instanceof ExternalLocation) {
                    ExternalLocation parentExternalLocation = (ExternalLocation) parentTreeNode.getUserObject();
                    ExternalLocation childExternalLocation = (ExternalLocation) ((DefaultMutableTreeNode) child).getUserObject();

                    int index = 0;
                    for (ExternalLocation childrenChild : parentExternalLocation.getChildren()) {
                        if (childrenChild.getId().equals(childExternalLocation.getId())) {
                            return index;
                        }
                        index++;
                    }
                    return -1;
                }
            }
            return super.getIndexOfChild(parent, child);
        }

        @Override
        public Object getRoot() {
            return super.getRoot();
        }

        @Override
        public boolean isLeaf(Object node) {
            if (node.equals(this.root)) {
                return false;
            } else {
                DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) node;
                if (treeNode.getUserObject() instanceof ExternalLocation) {
                    ExternalLocation externalLocation = (ExternalLocation) treeNode.getUserObject();
                    return externalLocation.getChildren().isEmpty();
                }
            }
            return super.isLeaf(node);
        }

        @Override
        public void valueForPathChanged(TreePath path, Object newValue) {
            super.valueForPathChanged(path, newValue);
        }

        public TreeNode getNodeForExternalLocation(ExternalLocation location) {
            if (locationTreeNodes.containsKey(location)) {
                return locationTreeNodes.get(location);
            } else {
                TreeNode node = new DefaultMutableTreeNode(location);
                locationTreeNodes.put(location, node);
                return node;
            }
        }
    }

    public class ExternalLocationTreeTableNode extends DefaultMutableTreeTableNode {

        public ExternalLocationTreeTableNode(Object userObject) {
            super(userObject);
        }

        public ExternalLocation getLocation() {
            return (ExternalLocation) getUserObject();
        }

        @Override
        public String toString() {
            return getLocation().getIdentifier();
        }
    }

    public class ExternalLocationTreeTableModel extends DefaultTreeTableModel {

        private Map<ExternalLocation, ExternalLocationTreeTableNode> nodes;
        public String[] columnHeadings = new String[]{
            "Location",
            "Level",
            "Description",
            "Status",
            "Classification",
            "Criticality",
            "Owner",
            "Account",
            "Doc#",
            "Drawing#",
            "Product",
            "Parent",
            "Assets",
            "Schedules"
        };

        public ExternalLocationTreeTableModel() {
            this(new DefaultMutableTreeTableNode("Root Node"));
        }

        public ExternalLocationTreeTableModel(DefaultMutableTreeTableNode rootNode) {
            this.nodes = new HashMap<ExternalLocation, ExternalLocationTreeTableNode>();
            this.root = rootNode;
        }

        public void reset() {
            this.nodes = new HashMap<ExternalLocation, ExternalLocationTreeTableNode>();
            DefaultMutableTreeTableNode rootNode = (DefaultMutableTreeTableNode) root;
            int childrenCount = rootNode.getChildCount();
            for (int i = 0; i < childrenCount; i++) {
                rootNode.remove(0);
            }
        }

        @Override
        public int getColumnCount() {
            return columnHeadings.length;
        }

        @Override
        public String getColumnName(int column) {
            return columnHeadings[column];
        }

        @Override
        public Object getValueAt(Object o, int i) {
            if (o.equals(root)) {
                return "Root Node";
            } else if (o instanceof ExternalLocationTreeTableNode) {
                ExternalLocationTreeTableNode node = (ExternalLocationTreeTableNode) o;
                ExternalLocation location = node.getLocation();
                switch (i) {
                    case 0:
                        return location.getIdentifier();
                    case 1:
                        return location.getLevel();
                    case 2:
                        return location.getDescription();
                    case 3:
                        return location.getStatus();
                    case 4:
                        return location.getClassification();
                    case 5:
                        return location.getCriticality();
                    case 6:
                        return location.getOwner();
                    case 7:
                        return location.getCostAccount();
                    case 8:
                        return location.getDocReference();
                    case 9:
                        return location.getDrawingReference();
                    case 10:
                        return location.getProduct();
                    case 11:
                        return (location.getParent() != null) ? location.getParent().getIdentifier() : "";
                    case 12:
                        return location.getAssets().size();
                    case 13:
                        return location.getSchedules().size();
                    default:
                        return "TODO";
                }
            } else {
                return o.toString();
            }
        }

        @Override
        public boolean isLeaf(Object o) {
            if (o.equals(root)) {
                return ((DefaultMutableTreeTableNode) o).getChildCount() == 0;
            } else {
                ExternalLocationTreeTableNode node = (ExternalLocationTreeTableNode) o;
                return node.getChildCount() == 0;
            }
        }

        public ExternalLocationTreeTableNode getExternalLocationTreeTableNode(ExternalLocation location) {
            if (nodes.containsKey(location)) {
                return nodes.get(location);
            } else {
                ExternalLocationTreeTableNode node = new ExternalLocationTreeTableNode(location);
                nodes.put(location, node);
                return node;
            }
        }

        @Override
        public boolean isCellEditable(Object node, int column) {
            return false;
        }
    }

    public class ExternalAssetTreeTableNode extends DefaultMutableTreeTableNode {

        public ExternalAssetTreeTableNode(Object userObject) {
            super(userObject);
        }

        public ExternalAsset getAsset() {
            return (ExternalAsset) getUserObject();
        }

        @Override
        public String toString() {
            return getAsset().getIdentifier();
        }
    }

    public class ExternalAssetTreeTableModel extends DefaultTreeTableModel {

        private Map<ExternalAsset, ExternalAssetTreeTableNode> nodes;
        public String[] columnHeadings = new String[]{
            "Asset",
            "Location",
            "Description",
            "Status",
            "Failure Code",
            "Classification",
            "Manufacturer",
            "Model",
            "Serial",
            "Arrangement"
        };

        public ExternalAssetTreeTableModel() {
            this(new DefaultMutableTreeTableNode("Root Node"));
        }

        public ExternalAssetTreeTableModel(DefaultMutableTreeTableNode rootNode) {
            this.nodes = new HashMap<ExternalAsset, ExternalAssetTreeTableNode>();
            this.root = rootNode;
        }

        public void reset() {
            this.nodes = new HashMap<ExternalAsset, ExternalAssetTreeTableNode>();
            DefaultMutableTreeTableNode rootNode = (DefaultMutableTreeTableNode) root;
            int childrenCount = rootNode.getChildCount();
            for (int i = 0; i < childrenCount; i++) {
                rootNode.remove(0);
            }
        }

        @Override
        public int getColumnCount() {
            return columnHeadings.length;
        }

        @Override
        public String getColumnName(int column) {
            return columnHeadings[column];
        }

        @Override
        public Object getValueAt(Object o, int i) {
            if (o.equals(root)) {
                return "Root Node";
            } else if (o instanceof ExternalAssetTreeTableNode) {
                ExternalAssetTreeTableNode node = (ExternalAssetTreeTableNode) o;
                ExternalAsset asset = node.getAsset();
                switch (i) {
                    case 0:
                        return asset.getIdentifier();
                    case 1:
                        return asset.getLocation().getIdentifier();
                    case 2:
                        return asset.getDescription();
                    case 3:
                        return asset.getStatus();
                    case 4:
                        return asset.getFailureCode();
                    case 5:
                        return asset.getClassification();
                    case 6:
                        return asset.getManufacturer();
                    case 7:
                        return asset.getModel();
                    case 8:
                        return asset.getSerialNumber();
                    case 9:
                        return asset.getArrangement();

                    default:
                        return "TODO";
                }
            } else {
                return o.toString();
            }
        }

        @Override
        public boolean isLeaf(Object o) {
            if (o.equals(root)) {
                return ((DefaultMutableTreeTableNode) o).getChildCount() == 0;
            } else {
                ExternalAssetTreeTableNode node = (ExternalAssetTreeTableNode) o;
                return node.getChildCount() == 0;
            }
        }

        public ExternalAssetTreeTableNode getExternalAssetTreeTableNode(ExternalAsset asset) {
            if (nodes.containsKey(asset)) {
                return nodes.get(asset);
            } else {
                ExternalAssetTreeTableNode node = new ExternalAssetTreeTableNode(asset);
                nodes.put(asset, node);
                return node;
            }
        }

        @Override
        public boolean isCellEditable(Object node, int column) {
            return false;
        }
    }

    public class ExternalScheduleTreeTableNode extends DefaultMutableTreeTableNode {

        public ExternalScheduleTreeTableNode(Object userObject) {
            super(userObject);
        }

        public ExternalSchedule getSchedule() {
            return (ExternalSchedule) getUserObject();
        }

        @Override
        public String toString() {
            return getSchedule().getIdentifier();
        }
    }

    public class ExternalScheduleTreeTableModel extends DefaultTreeTableModel {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
        private HashMap<ExternalSchedule, ExternalScheduleTreeTableNode> nodes;
        public String[] columnHeadings = new String[]{
            "Schedule",
            "Description",
            "Location",
            "Asset",
            "Status",
            "Next Date",
            "Extended Date",
            "Frequency",
            "Unit"
        };

        public ExternalScheduleTreeTableModel() {
            this(new DefaultMutableTreeTableNode("Root Node"));
        }

        public ExternalScheduleTreeTableModel(DefaultMutableTreeTableNode rootNode) {
            this.nodes = new HashMap<ExternalSchedule, ExternalScheduleTreeTableNode>();
            this.root = rootNode;
        }

        public void reset() {
            this.nodes = new HashMap<ExternalSchedule, ExternalScheduleTreeTableNode>();
            DefaultMutableTreeTableNode rootNode = (DefaultMutableTreeTableNode) root;
            int childrenCount = rootNode.getChildCount();
            for (int i = 0; i < childrenCount; i++) {
                rootNode.remove(0);
            }
        }

        @Override
        public int getColumnCount() {
            return columnHeadings.length;
        }

        @Override
        public String getColumnName(int column) {
            return columnHeadings[column];
        }

        @Override
        public Object getValueAt(Object o, int i) {
            if (o.equals(root)) {
                return "Root Node";
            } else if (o instanceof ExternalScheduleTreeTableNode) {
                ExternalScheduleTreeTableNode node = (ExternalScheduleTreeTableNode) o;
                ExternalSchedule schedule = node.getSchedule();
                switch (i) {
                    case 0:
                        return schedule.getIdentifier();
                    case 1:
                        return schedule.getDescription();
                    case 2:
                        return (schedule.getLocation() != null) ? schedule.getLocation().getIdentifier() : "";
                    case 3:
                        return (schedule.getAsset() != null) ? schedule.getAsset().getIdentifier() : "";
                    case 4:
                        return schedule.getStatus();
                    case 5:
                        return (schedule.getNextDueDate() != null) ? dateFormat.format(new Date(schedule.getNextDueDate().getTime())) : "";
                    case 6:
                        return (schedule.getExtendedDate() != null) ? dateFormat.format(new Date(schedule.getExtendedDate().getTime())) : "";
                    case 7:
                        return schedule.getFrequency();
                    case 8:
                        return schedule.getFrequencyUnit();
                    default:
                        return "TODO";
                }
            } else {
                return o.toString();
            }
        }

        @Override
        public boolean isLeaf(Object o) {
            if (o.equals(root)) {
                return ((DefaultMutableTreeTableNode) o).getChildCount() == 0;
            } else {
                ExternalScheduleTreeTableNode node = (ExternalScheduleTreeTableNode) o;
                return node.getChildCount() == 0;
            }
        }

        public ExternalScheduleTreeTableNode getExternalScheduleTreeTableNode(ExternalSchedule schedule) {
            if (nodes.containsKey(schedule)) {
                return nodes.get(schedule);
            } else {
                ExternalScheduleTreeTableNode node = new ExternalScheduleTreeTableNode(schedule);
                nodes.put(schedule, node);
                return node;
            }
        }

        @Override
        public boolean isCellEditable(Object node, int column) {
            return false;
        }
    }
}
