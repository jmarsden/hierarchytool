/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me.app;

import com.santos.me.PropertyUtils;
import com.santos.me.controller.AppController;
import com.santos.me.controller.DatabaseToolsController;
import com.santos.me.controller.ExternalViewController;
import com.santos.me.controller.LoginDialogController;
import com.santos.me.model.ExternalViewModel;
import com.santos.me.view.DatabaseToolsDialog;
import com.santos.me.view.ExternalViewPanel;
import com.santos.me.view.LoginDialog;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import org.jdesktop.swingx.JXFrame;

public class Main {

    private static String OS = System.getProperty("os.name").toLowerCase();
    private JXFrame frame;
    private AppController controller;
    JTabbedPane tabbedPane;

    public Main() {
        frame = null;

        tabbedPane = null;

        controller = new AppController();
    }

    public static void main(String[] args) {
        try {
            if (isWindows() || isMac()) {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } else {
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Nimbus".equals(info.getName())) {
                        UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(Object.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        Main main = new Main();
        main.init();
        main.startAppplication();
    }

    private void init() {
        URL icon16 = Object.class.getResource("/pipe16.png");
        URL icon32 = Object.class.getResource("/pipe32.png");
        URL icon48 = Object.class.getResource("/pipe48.png");
        URL icon128 = Object.class.getResource("/pipe128.png");

        Toolkit kit = Toolkit.getDefaultToolkit();
        List<Image> icons = new ArrayList<Image>();
        icons.add(kit.getImage(icon16));
        icons.add(kit.getImage(icon32));
        icons.add(kit.getImage(icon48));
        icons.add(kit.getImage(icon128));

        frame = new JXFrame("Maintenance Excellence: Hierarchy Tool - *This tool is not supported by the Service Centre*");
        frame.setSize(800, 600);
        frame.setJMenuBar(new MenuBarComponent());

        frame.setIconImages(icons);

        frame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });

        ExternalViewModel externalViewModel = new ExternalViewModel();
        ExternalViewController externalViewController = new ExternalViewController(externalViewModel);
        ExternalViewPanel externalViewPanel = new ExternalViewPanel(externalViewController);

        tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Hierarchy", externalViewPanel);

        frame.getContentPane().add(tabbedPane);
    }

    private void startAppplication() {
        frame.setVisible(true);
        LoginDialog loginDialog = new LoginDialog(new LoginDialogController(), frame, true);
        loginDialog.setResizable(false);
        loginDialog.setTitle("Login Dialog");
        loginDialog.setLocationRelativeTo(frame);
        try {
            loginDialog.setProperties(PropertyUtils.loadMergedProperties());
        } catch (MalformedURLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

        loginDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                super.windowClosed(e);
                System.exit(0);
            }
        });
        loginDialog.setVisible(true);

    }

    public class MenuBarComponent extends JMenuBar {

        JMenu fileMenu, toolsMenu, helpMenu;
        JMenuItem exitMenuItem, createDatabaseMenuItem;

        @Override
        public void addNotify() {
            super.addNotify();

            fileMenu = new JMenu("File");
            fileMenu.setMnemonic('F');

            exitMenuItem = new JMenuItem("Exit", KeyEvent.VK_E);
            exitMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    System.exit(0);
                }
            });

            fileMenu.add(exitMenuItem);

            toolsMenu = new JMenu("Tools");
            toolsMenu.setMnemonic('T');

            createDatabaseMenuItem = new JMenuItem("Create Database Tool", KeyEvent.VK_C);
            createDatabaseMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DatabaseToolsController controller = new DatabaseToolsController();
                    DatabaseToolsDialog dialog = new DatabaseToolsDialog(controller, frame, true);
                    //dialog.setSize(680, 450);
                    dialog.setLocationRelativeTo(frame);

                    dialog.setVisible(true);
                }
            });

            toolsMenu.add(createDatabaseMenuItem);

            add(fileMenu);
            add(toolsMenu);
        }
    }

    public static boolean isWindows() {
        return (OS.indexOf("win") >= 0);

    }

    public static boolean isMac() {
        return (OS.indexOf("mac") >= 0);

    }

    public static boolean isUnix() {
        return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);

    }

    public static boolean isSolaris() {
        return (OS.indexOf("sunos") >= 0);
    }
}
