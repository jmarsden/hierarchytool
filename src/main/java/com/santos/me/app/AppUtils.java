/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santos.me.app;

import java.util.Properties;

/**
 *
 * @author jmarsden
 */
public class AppUtils {

    private static boolean isInitialised;
    private static Properties properties;

    static {
        isInitialised = false;
        properties = null;
    }

    public synchronized static void init(Properties props) {
        if(isInitialised) {
            throw new RuntimeException("Already Initialised - you likely dont want to do this twice.");
        }
        isInitialised = true;
        properties = props;
        
    }
    
    public static Properties getProperties() {
        return properties;
    }
}
