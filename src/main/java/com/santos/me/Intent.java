/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me;

import com.santos.me.Controller;
import java.util.ArrayList;
import java.util.List;

public abstract class Intent {
	
	public final Controller source;
	public List<IntentReceiver> receivers;
	public final int topic;
	
	public Intent(Controller source) {
		this.source = source;
		this.receivers = null;
		this.topic = IntentRegistry.lookupSingleton().registerIntent(this);
	}

	/**
	 * @return the source
	 */
	public Controller getSource() {
		return source;
	}

	/**
	 * @return the topic
	 */
	public int getTopic() {
		return topic;
	}
	
	public void addReceiver(IntentReceiver component) {
		if(receivers == null) {
			receivers = new ArrayList<IntentReceiver>();
		}
		receivers.add(component);
	}
	
	public boolean wasReceived() {
		if(receivers != null && !receivers.isEmpty()) {
			return true;
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		//[Receivers=\"" + ((receivers == null) ? 0 : receivers.size()) + "\"]
		return "Intent [Source=" + source + " Topic=" + getTopic() + "]";
	}
}
