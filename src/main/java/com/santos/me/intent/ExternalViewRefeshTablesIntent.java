/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me.intent;

import com.santos.me.Controller;
import com.santos.me.Intent;
import com.santos.me.objects.ExternalLocation;

/**
 *
 * @author jmarsden
 */
public class ExternalViewRefeshTablesIntent extends Intent {

    ExternalLocation location;
    int levels;

    public ExternalViewRefeshTablesIntent(Controller source, ExternalLocation location, int levels) {
        super(source);
        this.location = location;
        this.levels = levels;
    }

    public ExternalLocation getLocation() {
        return location;
    }

    public void setLocation(ExternalLocation location) {
        this.location = location;
    }

    public int getLevels() {
        return levels;
    }

    public void setLevels(int levels) {
        this.levels = levels;
    }
}
