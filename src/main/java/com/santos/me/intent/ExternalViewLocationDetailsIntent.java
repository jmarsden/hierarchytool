/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me.intent;

import com.santos.me.Controller;
import com.santos.me.Intent;
import com.santos.me.objects.ExternalLocation;
import java.util.List;

/**
 *
 * @author jmarsden
 */
public class ExternalViewLocationDetailsIntent extends Intent {

    List<ExternalLocation> locations;

    public ExternalViewLocationDetailsIntent(Controller source, List<ExternalLocation> locations) {
        super(source);
        this.locations = locations;
    }

    public List<ExternalLocation> getLocations() {
        return locations;
    }

    public void setLocations(List<ExternalLocation> locations) {
        this.locations = locations;
    }
    
}
