/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me.model;

import com.santos.me.database.HibernateUtil;
import com.santos.me.Model;
import com.santos.me.objects.ExternalLocation;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author jmarsden
 */
public class ExternalViewModel extends Model {

    private Session session;
    
    // TODO: Make Roots Data Driven

    private String[] rootLocationIdentifiers = new String[]{"COOPER", "NT", "VIC", "PTBONY", "NSW", "EQ", "GRATI"};
    private List<ExternalLocation> rootsLocations;

    public ExternalViewModel() {

        
        rootsLocations = null;
        session = null;
    }

    public void checkSession() {
        if (session == null) {
            if(!HibernateUtil.isConnected()) {
                try {
                    HibernateUtil.connect();
                } catch (MalformedURLException ex) {
                    Logger.getLogger(ExternalViewModel.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ExternalViewModel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            session = HibernateUtil.getSessionFactory().openSession();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        session.close();
        super.finalize();
    }

    public List<ExternalLocation> getExternalLocationRoots() {
        if (rootsLocations != null) {
            return rootsLocations;
        } else {
            Properties properties = HibernateUtil.getProperties();
            
            
            rootsLocations = new ArrayList<ExternalLocation>();
            StringBuilder inValue = new StringBuilder();
            for (int i = 0; i < rootLocationIdentifiers.length - 1; i++) {
                inValue.append("'").append(rootLocationIdentifiers[i]).append("',");
            }
            inValue.append("'").append(rootLocationIdentifiers[rootLocationIdentifiers.length - 1]).append("'");

            Query query = session.createQuery("from ExternalLocation as exLocations where exLocations.identifier in (" + inValue.toString() + ")");
            List<ExternalLocation> rootExternalLocations = query.list();
            for (ExternalLocation loc : rootExternalLocations) {
                rootsLocations.add(loc);
            }
            return rootsLocations;
        }
    }

    public void reset() {
        checkSession();
        rootsLocations = null;
    }
}
