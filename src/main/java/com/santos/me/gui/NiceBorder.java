/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.border.AbstractBorder;

public class NiceBorder extends AbstractBorder {

    protected Color lineColor;
    protected boolean hasTop, hasLeft, hasBottom, hasRight;
    protected int widthTop, widthLeft, widthBottom, widthRight;
    protected Insets insets;
    
    public NiceBorder(Color color, Insets insets, boolean top, boolean left, boolean bottom, boolean right) {
        lineColor = color;
        this.insets = insets;
        hasTop = top;
        hasLeft = left;
        hasBottom = bottom;
        hasRight = right;
        widthTop = 1;
        widthLeft = 1;
        widthBottom = 1;
        widthRight = 1;
    }
    
    public boolean hasBottom() {
        return hasBottom;
    }

    public void setHasBottom(boolean hasBottom) {
        this.hasBottom = hasBottom;
    }

    public boolean hasLeft() {
        return hasLeft;
    }

    public void setHasLeft(boolean hasLeft) {
        this.hasLeft = hasLeft;
    }

    public boolean hasRight() {
        return hasRight;
    }

    public void setHasRight(boolean hasRight) {
        this.hasRight = hasRight;
    }

    public boolean hasTop() {
        return hasTop;
    }

    public void setHasTop(boolean hasTop) {
        this.hasTop = hasTop;
    }

    public Insets getInsets() {
        return insets;
    }

    public void setInsets(Insets insets) {
        this.insets = insets;
    }

    public Color getLineColor() {
        return lineColor;
    }

    public void setLineColor(Color lineColor) {
        this.lineColor = lineColor;
    }

    public int getWidthBottom() {
        return widthBottom;
    }

    public void setWidthBottom(int widthBottom) {
        this.widthBottom = widthBottom;
    }

    public int getWidthLeft() {
        return widthLeft;
    }

    public void setWidthLeft(int widthLeft) {
        this.widthLeft = widthLeft;
    }

    public int getWidthRight() {
        return widthRight;
    }

    public void setWidthRight(int widthRight) {
        this.widthRight = widthRight;
    }

    public int getWidthTop() {
        return widthTop;
    }

    public void setWidthTop(int widthTop) {
        this.widthTop = widthTop;
    }


    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        Color color = g.getColor();
                
        g.setColor(lineColor);

        // top
        if (hasTop) {
            g.drawLine(x, y, width - 1, y);
        }

        // left
        if (hasLeft) {
            g.drawLine(x, y, x, height - 1);
        }

        // bottom
        if (hasBottom) {
            g.drawLine(x, height - 1, width - 1, height - 1);
        }

        // right
        if (hasRight) {
            g.drawLine(width - 1, y, width - 1, height - 1);
        }
        
        g.setColor(color);
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return (Insets) insets.clone();
    }

    @Override
    public boolean isBorderOpaque() {
        return false;
    }
}