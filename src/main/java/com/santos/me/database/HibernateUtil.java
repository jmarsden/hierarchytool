/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me.database;

import com.santos.me.PropertyUtils;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.H2Dialect;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;

/**
 *
 * @author jmarsden
 */
public class HibernateUtil {

    public static final String[] resources = new String[] {
        "com.santos.me.objects.ExternalLocation",
        "com.santos.me.objects.ExternalAsset",
        "com.santos.me.objects.ExternalSchedule",
        "com.santos.me.objects.ExternalWork",
        "com.santos.me.objects.ExternalItem",
        "com.santos.me.objects.ExternalSparePart",
        
        "com.santos.me.objects.ChangeSet",
        "com.santos.me.objects.LocationChange"
    };

    private static boolean connected;
    private static Properties properties;
    private static AnnotationConfiguration configuration;
    private static SessionFactory sessionFactory;
    private static Dialect dialect;

    static {
        connected = false;
        properties = null;
        configuration = null;
        sessionFactory = null;
        dialect = null;
    }

    public synchronized static void connect() throws MalformedURLException, IOException {
        connect(PropertyUtils.loadMergedProperties());
    }

    public synchronized static void connect(Properties props) {
        
        properties = props;
        
        dialect = new H2Dialect();

        configuration = new AnnotationConfiguration();

        configuration.setProperties(properties);

        for(String resource: resources) {
            try {
                configuration.addAnnotatedClass(Class.forName(resource));
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(HibernateUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        sessionFactory = configuration.buildSessionFactory();

        connected = true;
    }

    public static void checkDatabase() {
        if (!connected) {
            throw new RuntimeException("Database Not Connected");
        }
        SchemaUpdate update = new SchemaUpdate(configuration);
        update.execute(false, true);
    }

    public static Properties getProperties() {
        return properties;
    }
    
    public static Configuration getConfiguration() {
        return configuration;
    }

    public static SessionFactory getSessionFactory() {
        if (!connected) {
            throw new RuntimeException("Database Not Connected");
        }
        return sessionFactory;
    }

    public static Dialect getDialect() {
        return dialect;
    }

    public synchronized static void shutdown() {
        if(!connected) {
            return;
        }
        
        sessionFactory.close();
        connected = false;
    }

    public static boolean isConnected() {
        return connected;
    }
}
