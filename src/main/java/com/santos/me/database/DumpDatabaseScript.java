/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me.database;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;

/**
 *
 * @author jmarsden
 */
public class DumpDatabaseScript {

    private void dumpScriptData() {
        try {
            HibernateUtil.connect();
        } catch (MalformedURLException ex) {
            Logger.getLogger(DumpDatabaseScript.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DumpDatabaseScript.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Configuration configuration = HibernateUtil.getConfiguration();
        SchemaExport schema = new SchemaExport(configuration);
        schema.drop(true, false);

        schema.create(true, false);

        SchemaUpdate update = new SchemaUpdate(configuration);
        update.execute(true, false);
        
        HibernateUtil.shutdown();
    }

    public static void main(String[] args) throws SQLException {
        DumpDatabaseScript script = new DumpDatabaseScript();
        script.dumpScriptData();
    }
}
