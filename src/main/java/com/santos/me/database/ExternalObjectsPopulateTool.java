/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me.database;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;

/**
 *
 * @author jmarsden
 */
public class ExternalObjectsPopulateTool {

    private Properties properties;

    public ExternalObjectsPopulateTool() {
        this.properties = null;
        try {
            HibernateUtil.connect();
        } catch(MalformedURLException ex) {
            Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.SEVERE, null, ex);
        } catch(IOException ex) {
            Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ExternalObjectsPopulateTool(Properties properties) {
        this.properties = properties;
        HibernateUtil.connect(properties);
    }

    public void importData(int step) {
        System.out.println("Creating Schema:");
        createSchema();
        Properties props = HibernateUtil.getProperties();

        Connection connection = null;
        try {

            String dsn = props.getProperty("external.database.dsn");
            if(dsn != null) {
                connection = getConnectionDSN(dsn);
            } else {
                connection = getConnection(props);
            }

            importExternalLocations(connection, props);
            importExternalLocationHierarchy(connection, props);
            importExternalAssets(connection, props);
            importExternalSchedule(connection, props);

            Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.INFO, "Completed Database Creation");

            HibernateUtil.shutdown();
        } catch(SQLException ex) {
            Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(connection != null) {
                try {
                    connection.close();
                } catch(SQLException ex) {
                }
            }
        }
    }

    private void createSchema() {
        Configuration configuration = HibernateUtil.getConfiguration();
        SchemaExport schema = new SchemaExport(configuration);
        schema.drop(true, true);

        //schema.create(true, true);

        SchemaUpdate update = new SchemaUpdate(configuration);
        update.execute(true, true);
    }

    private Connection getConnection(Properties databaseProperties) throws SQLException {
        try {
            Class.forName(databaseProperties.getProperty("external.database.driver"));
        } catch(ClassNotFoundException ex) {
            Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.SEVERE, null, ex);
        }

        Connection connection = DriverManager.getConnection(databaseProperties.getProperty("external.database.connection"), databaseProperties.getProperty("external.database.username"), databaseProperties.getProperty("external.database.password"));

        return connection;
    }

    public Connection getConnectionDSN(String dsnName) throws SQLException {
        try {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        } catch(ClassNotFoundException ex) {
            Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.SEVERE, null, ex);
        }

        String connection = "jdbc:odbc:" + dsnName + "";
        return DriverManager.getConnection(connection, "", "");
    }

    public void importExternalLocations(Connection connection, Properties properties) throws SQLException {
        String selectQuery = null;

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();

        Statement statement = null;
        ResultSet result = null;

        try {

            statement = connection.createStatement();

            selectQuery = "SELECT L.ID, L.identifier, L.SITEID, L.DESCRIPTION, CASE WHEN DESC_SUFFIX = ANCESTOR_DESC THEN 0 ELSE 1 END AS DESCINTFLAG, DESC_SUFFIX,  \n"
                    + "CASE WHEN DESC_SUFFIX = ANCESTOR_DESC OR L.STATUS IN ('ABANDONED') THEN NULL ELSE ANCESTOR_DESC END AS ANCESTOR_DESC, \n"
                    + "LD.LDTEXT AS DETAILS, L.COSTACCOUNT, L.DRAWINGREFERENCE, L.DOCREFERENCE, L.OWNER, \n"
                    + "L.CRITICALITY, UPPER(L.PRODUCT) AS PRODUCT, L.CLASSIFICATION, L.STATUS \n"
                    + "FROM \n"
                    + "( \n"
                    + "SELECT L.LOCATIONSID AS ID, L.LOCATION AS identifier, L.SITEID, \n"
                    + "L.GLACCOUNT AS COSTACCOUNT, L.PID AS DRAWINGREFERENCE, L.TIMSNUM AS DOCREFERENCE, L.CUSTODIANGROUP AS OWNER, \n"
                    + "L.CMLIKELIHOOD AS CRITICALITY, L.GISPARAM2 AS PRODUCT, L.CLASSSTRUCTUREID AS CLASSIFICATION, L.STATUS, L.HASLD, \n"
                    + "CASE  \n"
                    + "  WHEN INSTR(L.DESCRIPTION,';') = 0  \n"
                    + "    THEN L.DESCRIPTION  \n"
                    + "    ELSE SUBSTR(L.DESCRIPTION,0,INSTR(L.DESCRIPTION,';')-1)  \n"
                    + "END AS DESCRIPTION, \n"
                    + "CASE  \n"
                    + "  WHEN INSTR(L.DESCRIPTION,';') = 0  \n"
                    + "    THEN NULL \n"
                    + "    ELSE REPLACE(REPLACE(RTRIM(LTRIM(SUBSTR(L.DESCRIPTION,INSTR(L.DESCRIPTION,';')+1,LENGTH(L.DESCRIPTION)))),'  ', ' '),' ,',',')  \n"
                    + "END AS DESC_SUFFIX, \n"
                    + "CASE \n"
                    + "  WHEN AN.DESCRIPTION IS NULL OR INSTR(AN.DESCRIPTION,';') = 0  \n"
                    + "    THEN AN.DESCRIPTION  \n"
                    + "    ELSE REPLACE(REPLACE(SUBSTR(AN.DESCRIPTION,0,INSTR(AN.DESCRIPTION,';')-1) || ', ' || RTRIM(LTRIM(SUBSTR(AN.DESCRIPTION,INSTR(AN.DESCRIPTION,';')+1,LENGTH(AN.DESCRIPTION)))),'  ', ' '),' ,',',')  \n"
                    + "END AS ANCESTOR_DESC \n"
                    + "FROM " + properties.getProperty("external.database.tableprefix", "") + "LOCATIONS L \n"
                    + "LEFT JOIN " + properties.getProperty("external.database.tableprefix", "") + "LOCHIERARCHY LH ON L.LOCATION = LH.LOCATION AND L.SITEID = LH.SITEID AND LH.SYSTEMID = 'PRIMARY' \n"
                    + "LEFT JOIN " + properties.getProperty("external.database.tableprefix", "") + "LOCATIONS AN ON LH.PARENT = AN.LOCATION AND LH.SITEID = AN.SITEID \n"
                    + ") L \n"
                    + "LEFT JOIN " + properties.getProperty("external.database.tableprefix", "") + "LONGDESCRIPTION LD ON L.HASLD=1 AND LD.LDOWNERTABLE='LOCATIONS' AND LD.LDOWNERCOL='DESCRIPTION' AND L.ID = LD.LDKEY \n"
                    + "WHERE SITEID IN (" + properties.getProperty("external.database.sitelist", "'COOPER','PTBONY','VIC','NSW','NT'") + ")";

            Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.INFO, "SQL Select:{0}", selectQuery);

            result = statement.executeQuery(selectQuery);

            int count = 0;

            while(result.next()) {

                int id = result.getInt("ID");
                String identifier = result.getString("identifier");
                String siteId = result.getString("SITEID");
                String description = escape(result.getString("DESCRIPTION"));
                String descriptionSuffix = escape(result.getString("DESC_SUFFIX"));
                String ancestorDescription = escape(result.getString("ANCESTOR_DESC"));

                int descriptionIntegrityFlag = result.getInt("DESCINTFLAG");

                String details = result.getString("DETAILS");

                String costAccount = result.getString("COSTACCOUNT");
                String drawingReference = result.getString("DRAWINGREFERENCE");
                String docReference = result.getString("DOCREFERENCE");
                String owner = result.getString("OWNER");
                String criticality = result.getString("CRITICALITY");
                String classification = result.getString("CLASSIFICATION");
                String status = result.getString("STATUS");
                String product = result.getString("PRODUCT");

                String update = "INSERT INTO ExternalLocation (id, identifier, siteId, description, details, \r\n"
                        + "costAccount, drawingReference, docReference, owner, criticality, classification, status, product, \r\n"
                        + "descriptionSuffix, ancestorDescription, descriptionIntegrityFlag) \r\n"
                        + "VALUES ("
                        + id + ","
                        + "'" + identifier + "',"
                        + ((siteId == null) ? "null" : "'" + siteId + "'") + ","
                        + ((description == null) ? "null" : "'" + escape(description) + "'") + ","
                        + ((details == null) ? "null" : "'" + escape(details) + "'") + ","
                        + ((costAccount == null) ? "null" : "'" + costAccount + "'") + ","
                        + ((drawingReference == null) ? "null" : "'" + drawingReference + "'") + ","
                        + ((docReference == null) ? "null" : "'" + docReference + "'") + ","
                        + ((owner == null) ? "null" : "'" + owner + "'") + ","
                        + ((criticality == null) ? "null" : "'" + criticality + "'") + ","
                        + ((classification == null) ? "null" : "'" + classification + "'") + ","
                        + ((status == null) ? "null" : "'" + status + "'") + ","
                        + ((product == null) ? "null" : "'" + product + "'") + ","
                        + ((descriptionSuffix == null) ? "null" : "'" + descriptionSuffix + "'") + ","
                        + ((ancestorDescription == null) ? "null" : "'" + ancestorDescription + "'") + ","
                        + descriptionIntegrityFlag
                        + ")";

                session.createSQLQuery(update).executeUpdate();

                if(++count % 1000 == 0) {
                    session.flush();
                    transaction.commit();
                    session = HibernateUtil.getSessionFactory().openSession();
                    transaction = session.beginTransaction();

                    Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.INFO, "ExternalLocation Flushing Row {0} {1}", new Object[]{count, new java.util.Date()});
                }
            }
            session.flush();
            transaction.commit();

            Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.INFO, "ExternalLocation Flushing Row {0} {1}", new Object[]{count, new java.util.Date()});
        } catch(SQLException e) {
            throw e;
        } finally {
            if(result != null) {
                result.close();
            }
            if(statement != null) {
                statement.close();
            }
        }
    }

    private void importExternalLocationHierarchy(Connection connection, Properties properties) throws SQLException {
        String selectQuery = null;

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();


        Statement statement = null;
        ResultSet result = null;

        try {
            Transaction transaction = session.beginTransaction();

            statement = connection.createStatement();

            selectQuery = "SELECT location as childIdentifier, siteid, parent as parentIdentifier \r\n"
                    + "FROM " + properties.getProperty("external.database.tableprefix", "") + "LOCHIERARCHY \r\n"
                    + "WHERE SYSTEMID='PRIMARY' AND SITEID IN (" + properties.getProperty("external.database.sitelist", "'COOPER','PTBONY','VIC','NSW','NT'") + ")";

            Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.INFO, "SQL Select:{0}", selectQuery);

            result = statement.executeQuery(selectQuery);

            int count = 0;

            while(result.next()) {

                String childIdentifier = result.getString("childIdentifier");
                String parentIdentifier = result.getString("parentIdentifier");
                String siteId = result.getString("siteId");

                String update = "INSERT INTO EXTERNALLOCATIONHIERARCHY (child_Id, parent_Id) "
                        + "VALUES ("
                        + "SELECT ID FROM EXTERNALLOCATION WHERE identifier='" + childIdentifier + "' AND siteId='" + siteId + "',"
                        + "SELECT ID FROM EXTERNALLOCATION WHERE identifier='" + parentIdentifier + "' AND siteId='" + siteId + "'"
                        + ")";

                session.createSQLQuery(update).executeUpdate();

                if(++count % 1000 == 0) {
                    session.flush();
                    transaction.commit();
                    session = HibernateUtil.getSessionFactory().openSession();
                    transaction = session.beginTransaction();

                    Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.INFO, "ExternalLocationHierarchy Flushing Row {0} {1}", new Object[]{count, new java.util.Date()});
                }
            }
            session.flush();
            transaction.commit();
        } catch(SQLException e) {
            throw e;
        } finally {
            if(result != null) {
                result.close();
            }
            if(statement != null) {
                statement.close();
            }
        }
    }

    public void importExternalAssets(Connection connection, Properties properties) throws SQLException {
        String selectQuery = null;

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();

        Statement statement = null;
        ResultSet result = null;

        try {

            statement = connection.createStatement();

            selectQuery = "SELECT ASSETUID AS ID, ASSETNUM AS IDENTIFIER, SITEID, DESCRIPTION, LD.LDTEXT AS DETAILS, \r\n"
                    + "STATUS, classstructureid AS CLASSIFICATION, MANUFACTURER AS MANUFACTURER, SANTOSMODELNUM AS MODEL, \r\n"
                    + "SERIALNUM AS SERIALNUMBER, ARRANGEMENT, FAILURECODE, \r\n"
                    + "(SELECT LOCATIONSID FROM " + properties.getProperty("external.database.tableprefix", "") + "LOCATIONS L WHERE L.LOCATION = A.LOCATION AND L.SITEID = A.SITEID) AS LOCATION_ID \r\n"
                    + "FROM " + properties.getProperty("external.database.tableprefix", "") + "ASSET A \r\n"
                    + "LEFT JOIN " + properties.getProperty("external.database.tableprefix", "") + "LONGDESCRIPTION LD ON A.HASLD=1 AND A.ASSETUID = LD.LDKEY AND LD.LDOWNERTABLE = 'ASSET' AND LD.LDOWNERCOL = 'DESCRIPTION' \r\n"
                    + "WHERE A.SITEID IN (" + properties.getProperty("external.database.sitelist", "'COOPER','PTBONY','VIC','NSW','NT'") + ") AND LOCATION IS NOT NULL";

            Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.INFO, "SQL Select:{0}", selectQuery);

            result = statement.executeQuery(selectQuery);

            int count = 0;

            while(result.next()) {
                int id = result.getInt("ID");
                String identifier = result.getString("identifier");
                String siteId = result.getString("SITEID");
                String description = escape(result.getString("DESCRIPTION"));
                String details = result.getString("DETAILS");
                String status = result.getString("STATUS");
                String classification = result.getString("CLASSIFICATION");
                String manufacturer = result.getString("MANUFACTURER");
                String model = result.getString("MODEL");
                String serialNumber = result.getString("SERIALNUMBER");
                String arrangement = result.getString("ARRANGEMENT");
                String failureCode = result.getString("FAILURECODE");
                String locationId = result.getString("LOCATION_ID");

                //create table ExternalAsset (id bigint generated by default as identity, arrangement varchar(50), classification varchar(10), description varchar(255), details varchar(5000), failureCode varchar(50), indentifier varchar(50) not null, location binary(255), manufacturer varchar(50), model varchar(50), product varchar(4) not null, serialNumber varchar(25), siteId varchar(15) not null, status varchar(20), primary key (id), unique (indentifier, siteId))
                String update = "INSERT INTO EXTERNALASSET (ID, IDENTIFIER, SITEID, DESCRIPTION, DETAILS, STATUS, CLASSIFICATION, MANUFACTURER, MODEL, SERIALNUMBER, ARRANGEMENT, FAILURECODE, LOCATION_ID) \r\n"
                        + "VALUES ("
                        + id + ","
                        + "'" + identifier + "',"
                        + ((siteId == null) ? "null" : "'" + siteId + "'") + ","
                        + ((description == null) ? "null" : "'" + escape(description) + "'") + ","
                        + ((details == null) ? "null" : "'" + escape(details) + "'") + ","
                        + ((status == null) ? "null" : "'" + escape(status) + "'") + ","
                        + ((classification == null) ? "null" : "'" + escape(classification) + "'") + ","
                        + ((manufacturer == null) ? "null" : "'" + escape(manufacturer) + "'") + ","
                        + ((model == null) ? "null" : "'" + escape(model) + "'") + ","
                        + ((serialNumber == null) ? "null" : "'" + escape(serialNumber) + "'") + ","
                        + ((arrangement == null) ? "null" : "'" + escape(arrangement) + "'") + ","
                        + ((failureCode == null) ? "null" : "'" + escape(failureCode) + "'") + ","
                        + ((locationId == null) ? "null" : escape(locationId))
                        + ")";

                session.createSQLQuery(update).executeUpdate();

                if(++count % 10000 == 0) {
                    session.flush();
                    transaction.commit();
                    session = HibernateUtil.getSessionFactory().openSession();
                    transaction = session.beginTransaction();

                    Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.INFO, "ExternalAsset Flushing Row {0} {1}", new Object[]{count, new java.util.Date()});
                }
            }
        } catch(SQLException e) {
            throw e;
        } finally {
            if(result != null) {
                result.close();
            }
            if(statement != null) {
                statement.close();
            }
        }

        session.flush();
        transaction.commit();
    }

    public void importExternalSchedule(Connection connection, Properties properties) throws SQLException {
        String selectQuery = null;

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();

        Statement statement = null;
        ResultSet result = null;

        try {

            statement = connection.createStatement();

            selectQuery = "SELECT PMUID AS ID, PMNUM AS IDENTIFIER, SITEID, DESCRIPTION, LD.LDTEXT AS DETAILS, STATUS, \r\n"
                    + "NEXTDATE, EXTDATE AS EXTENDEDDATE, FREQUENCY, FREQUNIT,  \r\n"
                    + "(SELECT LOCATIONSID FROM MXES.LOCATIONS L WHERE L.LOCATION = PM.LOCATION AND L.SITEID = PM.SITEID) AS LOCATION_ID, \r\n"
                    + "(SELECT ASSETUID FROM MXES.ASSET A WHERE A.ASSETNUM = PM.ASSETNUM AND A.SITEID = PM.SITEID) AS ASSET_ID \r\n"
                    + "FROM MXES.PM \r\n"
                    + "LEFT JOIN MXES.LONGDESCRIPTION LD ON PM.HASLD=1 AND PM.PMUID = LD.LDKEY AND LD.LDOWNERTABLE = 'PM' AND LD.LDOWNERCOL = 'DESCRIPTION' \r\n"
                    + "WHERE PM.SITEID IN (" + properties.getProperty("external.database.sitelist", "'COOPER','PTBONY','VIC','NSW','NT'") + ")";

            Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.INFO, "SQL Select:{0}", selectQuery);

            result = statement.executeQuery(selectQuery);

            int count = 0;

            while(result.next()) {
                int id = result.getInt("ID");
                String identifier = result.getString("identifier");
                String siteId = result.getString("SITEID");
                String description = escape(result.getString("DESCRIPTION"));
                String details = result.getString("DETAILS");
                String status = result.getString("STATUS");
                String locationId = result.getString("LOCATION_ID");
                String assetId = result.getString("ASSET_ID");
                Date nextDueDate = result.getDate("NEXTDATE");
                Date extendedDate = result.getDate("EXTENDEDDATE");
                String frequency = result.getString("FREQUENCY");
                String frequencyUnit = result.getString("FREQUNIT");

                //create table ExternalAsset (id bigint generated by default as identity, arrangement varchar(50), classification varchar(10), description varchar(255), details varchar(5000), failureCode varchar(50), indentifier varchar(50) not null, location binary(255), manufacturer varchar(50), model varchar(50), product varchar(4) not null, serialNumber varchar(25), siteId varchar(15) not null, status varchar(20), primary key (id), unique (indentifier, siteId))
                String update = "INSERT INTO EXTERNALSCHEDULE (ID, IDENTIFIER, SITEID, DESCRIPTION, DETAILS, STATUS, LOCATION_ID, ASSET_ID, \r\n"
                        + "NEXTDUEDATE, EXTENDEDDATE, FREQUENCY, FREQUENCYUNIT) \r\n"
                        + "VALUES ("
                        + id + ","
                        + "'" + identifier + "',"
                        + ((siteId == null) ? "null" : "'" + siteId + "'") + ","
                        + ((description == null) ? "null" : "'" + escape(description) + "'") + ","
                        + ((details == null) ? "null" : "'" + escape(details) + "'") + ","
                        + ((status == null) ? "null" : "'" + escape(status) + "'") + ","
                        + ((locationId == null) ? "null" : escape(locationId)) + ","
                        + ((assetId == null) ? "null" : escape(assetId)) + ","
                        + ((nextDueDate == null) ? "null" : "'" + nextDueDate.toString() + "'") + ","
                        + ((extendedDate == null) ? "null" : "'" + extendedDate.toString() + "'") + ","
                        + ((frequency == null) ? "null" : escape(frequency)) + ","
                        + ((frequencyUnit == null) ? "null" : "'" + escape(frequencyUnit) + "'")
                        + ")";

                session.createSQLQuery(update).executeUpdate();

                if(++count % 10000 == 0) {
                    session.flush();
                    transaction.commit();
                    session = HibernateUtil.getSessionFactory().openSession();
                    transaction = session.beginTransaction();

                    Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.INFO, "ExternalSchedule Flushing Row {0} {1}", new Object[]{count, new java.util.Date()});
                }
            }
        } catch(SQLException e) {
            throw e;
        } finally {
            if(result != null) {
                result.close();
            }
            if(statement != null) {
                statement.close();
            }
        }

        session.flush();
        transaction.commit();
    }

    public void importExternalWork(Connection connection, Properties properties) throws SQLException {
        String selectQuery = null;

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();

        Statement statement = null;
        ResultSet result = null;

        try {

            statement = connection.createStatement();

            selectQuery = "";

            Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.INFO, "SQL Select:{0}", selectQuery);

            result = statement.executeQuery(selectQuery);

            int count = 0;

            while(result.next()) {
                int id = result.getInt("ID");
                String identifier = result.getString("identifier");
                String siteId = result.getString("SITEID");
                String description = escape(result.getString("DESCRIPTION"));
                String details = result.getString("DETAILS");
                String status = result.getString("STATUS");

                //create table ExternalAsset (id bigint generated by default as identity, arrangement varchar(50), classification varchar(10), description varchar(255), details varchar(5000), failureCode varchar(50), indentifier varchar(50) not null, location binary(255), manufacturer varchar(50), model varchar(50), product varchar(4) not null, serialNumber varchar(25), siteId varchar(15) not null, status varchar(20), primary key (id), unique (indentifier, siteId))
                String update = "INSERT INTO EXTERNALASSET (ID, IDENTIFIER, SITEID, DESCRIPTION, DETAILS, STATUS, CLASSIFICATION, MANUFACTURER, MODEL, SERIAL, ARRANGEMENT, FAILIURECODE) \r\n"
                        + "VALUES ("
                        + id + ","
                        + "'" + identifier + "',"
                        + ((siteId == null) ? "null" : "'" + siteId + "'") + ","
                        + ((description == null) ? "null" : "'" + escape(description) + "'") + ","
                        + ((details == null) ? "null" : "'" + escape(details) + "'") + ","
                        + ((status == null) ? "null" : "'" + escape(status) + "'") + ","
                        + ")";

                session.createSQLQuery(update).executeUpdate();

                if(++count % 10000 == 0) {
                    session.flush();
                    transaction.commit();
                    session = HibernateUtil.getSessionFactory().openSession();
                    transaction = session.beginTransaction();

                    Logger.getLogger(ExternalObjectsPopulateTool.class.getName()).log(Level.INFO, "ExternalWork Flushing Row {0} {1}", new Object[]{count, new java.util.Date()});
                }
            }
        } catch(SQLException e) {
            throw e;
        } finally {
            if(result != null) {
                result.close();
            }
            if(statement != null) {
                statement.close();
            }
        }

        session.flush();
        transaction.commit();
    }

    private String escape(String string) {
        if(string == null) {
            return null;
        }
        return string.replace("'", "''");
    }

    public static void main(String[] args) throws SQLException {
        ExternalObjectsPopulateTool createDatabase = new ExternalObjectsPopulateTool();

        int step = 0;
        if(args.length > 0 && args[0] != null) {
            String stepString = args[0];
            try {
                step = Integer.parseInt(stepString);
            } catch(Exception e) {
                System.out.println("Could not parse step: " + args[0] + " " + e.toString());
            }
        }

        createDatabase.importData(step);
    }
}
