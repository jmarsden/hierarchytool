/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santos.me.objects;

import javax.persistence.*;

@Entity
public class ExternalSparePart {
    @Id
    @GeneratedValue
    private Long id;
    
    @ManyToOne
    private ExternalLocation location;
    
    @OneToOne
    private ExternalItem item;
    
}
