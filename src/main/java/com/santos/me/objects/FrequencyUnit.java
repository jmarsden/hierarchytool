/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santos.me.objects;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 *
 * @author jmarsden
 */
public enum FrequencyUnit {

    DAYS("Days"),
    MONTHS("Months"),
    WEEKS("Weeks"),
    YEARS("Years");
    
    @Enumerated(EnumType.STRING)
    private String value;

    FrequencyUnit(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
