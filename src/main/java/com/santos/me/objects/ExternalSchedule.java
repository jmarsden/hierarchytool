/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me.objects;

import java.sql.Date;

import javax.persistence.*;

import org.hibernate.annotations.Index;

@Entity
public class ExternalSchedule {

    @Id
    @GeneratedValue
    private Long id;
    
    @Index(name = "commonIndexExternalSchedule")
    @Column(nullable = false, length = 50)
    private String identifier;
    
    @Index(name = "commonIndexExternalSchedule")
    @Column(nullable = false, length = 15)
    private String siteId;
    
    @Column(nullable = true, length = 255)
    private String description;
    
    @Basic(fetch = FetchType.LAZY)  
    @Column(nullable = true, columnDefinition="CLOB")
    private String details;
    
    @Column(nullable = true, length = 20)
    private String status;
    
    @Column(nullable = true)
    private Date nextDueDate;
    
    @Column(nullable = true)
    private Date extendedDate;
    
    @Column(nullable = false)
    private int frequency;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private FrequencyUnit frequencyUnit;
    
    @ManyToOne(
        fetch=FetchType.LAZY
    )
    private ExternalLocation location;
    
    @ManyToOne(
        fetch=FetchType.LAZY
    )
    private ExternalAsset asset;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }
    
    public ExternalAsset getAsset() {
        return asset;
    }

    public void setAsset(ExternalAsset asset) {
        this.asset = asset;
    }

    public ExternalLocation getLocation() {
        return location;
    }

    public void setLocation(ExternalLocation location) {
        this.location = location;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Date getExtendedDate() {
        return extendedDate;
    }

    public void setExtendedDate(Date extendedDate) {
        this.extendedDate = extendedDate;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public FrequencyUnit getFrequencyUnit() {
        return frequencyUnit;
    }

    public void setFrequencyUnit(FrequencyUnit frequencyUnit) {
        this.frequencyUnit = frequencyUnit;
    }

    public Date getNextDueDate() {
        return nextDueDate;
    }

    public void setNextDueDate(Date nextDueDate) {
        this.nextDueDate = nextDueDate;
    }
}
