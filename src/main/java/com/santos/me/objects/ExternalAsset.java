/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me.objects;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import org.hibernate.annotations.Index;

@Entity
@Table(name = "ExternalAsset",
uniqueConstraints = {
    @UniqueConstraint(columnNames = {"identifier", "siteId"})})
public class ExternalAsset implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    @Index(name = "commonIndexExternalAsset")
    @Column(nullable = false, length = 50)
    private String identifier;
    @Index(name = "commonIndexExternalAsset")
    @Column(nullable = false, length = 15)
    private String siteId;
    @Column(nullable = true, length = 255)
    private String description;
    @Basic(fetch = FetchType.LAZY)
    @Column(nullable = true, columnDefinition = "CLOB")
    private String details;
    @Column(nullable = true, length = 20)
    private String status;
    @Column(nullable = true, length = 10)
    private String classification;
    @Column(nullable = true, length = 100)
    private String manufacturer;
    @Column(nullable = true, length = 100)
    private String model;
    @Column(nullable = true, length = 100)
    private String serialNumber;
    @Column(nullable = true, length = 100)
    private String arrangement;
    @Column(nullable = true, length = 50)
    private String failureCode;
    
    @ManyToOne(fetch = FetchType.LAZY)
    private ExternalLocation location;
    
    @OneToMany(mappedBy = "parent",
    cascade = {
        CascadeType.PERSIST
    },
    fetch = FetchType.LAZY)
    private List<ExternalAsset> children;
    
    
    @ManyToOne(fetch = FetchType.LAZY)
    @Index(name = "parentId", columnNames = {"parentId"})
    @JoinTable(name = "ExternalAssetHierarchy",
    joinColumns = {
        @JoinColumn(name = "child_Id", referencedColumnName = "id")
    },
    inverseJoinColumns = {
        @JoinColumn(name = "parent_Id", referencedColumnName = "id")
    })
    private ExternalAsset parent;
    
    
    @OneToMany(mappedBy = "asset",
    fetch = FetchType.LAZY)
    private List<ExternalSchedule> externalSchedules;
    
    @OneToMany(mappedBy = "asset",
    fetch = FetchType.LAZY)
    private List<ExternalWork> externalWorkorders;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getArrangement() {
        return arrangement;
    }

    public void setArrangement(String arrangement) {
        this.arrangement = arrangement;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getFailureCode() {
        return failureCode;
    }

    public void setFailureCode(String failureCode) {
        this.failureCode = failureCode;
    }

    public ExternalLocation getLocation() {
        return location;
    }

    public void setLocation(ExternalLocation location) {
        this.location = location;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public ExternalAsset getParent() {
        return parent;
    }

    public void setParent(ExternalAsset parent) {
        this.parent = parent;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if(obj instanceof ExternalAsset) {
            final ExternalAsset other = (ExternalAsset) obj;
            if ((this.id == null && other.id == null) ||  this.id.equals(other.id)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
    
    
}
