/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me.objects;

import javax.persistence.*;
import org.hibernate.annotations.Index;

@Entity
public class ExternalWork {
    @Id
    @GeneratedValue
    private Long id;
    
    @Index(name = "commonIndexExternalSchedule")
    @Column(nullable = false, length = 50)
    private String identifer;
    
    @Index(name = "commonIndexExternalSchedule")
    @Column(nullable = false, length = 15)
    private String siteId;

    @Column(nullable = true, length = 255)
    private String description;
    
    @Basic(fetch = FetchType.LAZY)  
    @Column(nullable = true, columnDefinition="CLOB")
    private String details;
    
    @Column(nullable = true, length = 20)
    private String status;

    @ManyToOne(
        fetch=FetchType.LAZY
    )
    private ExternalLocation location;
    
    @ManyToOne(
        fetch=FetchType.LAZY
    )
    private ExternalAsset asset;    
}
