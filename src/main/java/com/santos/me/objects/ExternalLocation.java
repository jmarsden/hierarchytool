/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me.objects;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import org.hibernate.annotations.Index;

import static javax.persistence.EnumType.*;

@Entity
@Table(
    name="ExternalLocation",
    uniqueConstraints = {@UniqueConstraint(columnNames={"identifier", "siteId"})}
)
public class ExternalLocation implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    
    @Index(name="commonIndexExternalLocation")
    @Column(nullable=false, length=50)
    private String identifier;

    @Index(name="commonIndexExternalLocation")
    @Column(nullable=false, length=15)
    private String siteId;
    
    @Column(nullable=true, length=255)
    private String description;
    
    @Column(nullable=true, length=255)
    private String descriptionSuffix;
    
    @Column(nullable=true, length=255)
    private String ancestorDescription;
    
    @Column(nullable=true)
    private boolean descriptionIntegrityFlag;
    
    @Basic(fetch = FetchType.LAZY)  
    @Column(nullable = true, columnDefinition="CLOB")
    private String details;
    
    @Column(nullable=true, length=20)
    private String status;
    
    @Column(nullable=true, length=10)
    private String classification;
    
    @Column(nullable=true, length=1)
    private String criticality;
    
    @Column(nullable=true, length=10)
    private String owner;
    @Column(nullable=true)
    private String costAccount;
    
    @Column(nullable=true, length=50)
    private String manufacturer;
    @Column(nullable=true, length=50)
    private String model;
    
    @Column(nullable=true, length=50)
    private String docReference;
    @Column(nullable=true, length=50)
    private String drawingReference;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable=true, length=4)
    Product product;
    
    @Column(nullable=true, length=50)
    private String assetGroup;
    
    @OneToMany(
        mappedBy = "parent", 
        cascade = {
            CascadeType.PERSIST
        },
        fetch=FetchType.LAZY
    )
    private List<ExternalLocation> children;
    
    @ManyToOne(
        fetch=FetchType.LAZY
    )
    @Index(name="parentId", columnNames={"parentId"})
    @JoinTable (
        name = "ExternalLocationHierarchy",
        joinColumns = {
            @JoinColumn(name="child_Id", referencedColumnName="id")
        },
        inverseJoinColumns = {
            @JoinColumn(name="parent_Id", referencedColumnName="id")
        }
    )
    private ExternalLocation parent;
    
    @OneToMany(
        mappedBy = "location",
        fetch=FetchType.EAGER
    )
    private List<ExternalAsset> assets;
    
    @OneToMany(
        mappedBy = "location",
        fetch=FetchType.LAZY
    )
    private List<ExternalSchedule> schedules;
    
    @OneToMany(
        mappedBy = "location",
        fetch=FetchType.LAZY
    )
    private List<ExternalWork> workorders;
    
    @Transient
    private int level;
    
    @OneToMany(
        mappedBy = "location",
        fetch=FetchType.LAZY
    )
    private List<ExternalSparePart> spareParts;
    
    public ExternalLocation() {
        this.id = null;
        this.children = null;
        this.parent = null;
        this.level = -1;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ExternalAsset> getAssets() {
        return assets;
    }

    public void setAssets(List<ExternalAsset> assets) {
        this.assets = assets;
    }

    public String getCostAccount() {
        return costAccount;
    }

    public void setCostAccount(String costAccount) {
        this.costAccount = costAccount;
    }

    public String getCriticality() {
        return criticality;
    }

    public void setCriticality(String criticality) {
        this.criticality = criticality;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDocReference() {
        return docReference;
    }

    public void setDocReference(String docReference) {
        this.docReference = docReference;
    }

    public String getDrawingReference() {
        return drawingReference;
    }

    public void setDrawingReference(String drawingReference) {
        this.drawingReference = drawingReference;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public List<ExternalLocation> getChildren() {
        return children;
    }

    public void setChildren(List<ExternalLocation> children) {
        this.children = children;
    }

    public ExternalLocation getParent() {
        return parent;
    }

    public void setParent(ExternalLocation parent) {
        this.parent = parent;
    }
    
    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public List<ExternalSchedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<ExternalSchedule> schedules) {
        this.schedules = schedules;
    }

    public List<ExternalWork> getWorkorders() {
        return workorders;
    }

    public void setWorkorders(List<ExternalWork> workorders) {
        this.workorders = workorders;
    }
    
    public int getLevel() {
        if(level != -1) {
            return level;
        } else if(getParent() == null) {
            level = 1;
            return level;
        } else {
            level = getParent().getLevel() + 1;
            return level;
        }
    }
   
    @Override
    public String toString() {
        return identifier + " : " + description;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if(obj instanceof ExternalLocation) {
            final ExternalLocation other = (ExternalLocation) obj;
            if ((this.id == null && other.id == null) ||  this.id.equals(other.id)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
   
}
