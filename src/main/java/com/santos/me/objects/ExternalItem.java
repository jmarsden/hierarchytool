/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santos.me.objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class ExternalItem {
    @OneToOne(mappedBy = "item")
    private ExternalSparePart item;

    @Id
    @GeneratedValue
    private Long id;
    
    private String manufacturer;
    
    private String catalogueCode;
    
    
}
