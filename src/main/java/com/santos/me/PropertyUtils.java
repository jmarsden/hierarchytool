/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.santos.me;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

/**
 *
 * @author jmarsden
 */
public class PropertyUtils {

    public static final String BASE_PROPERTIES_FILE = "base.properties";
    public static final String CONFIGURATION_PROPERTIES_FILE = "configuration.properties";
    
    public static final String INTERNAL_DATABASE_DRIVER_CLASS = "hibernate.connection.driver_class";
    public static final String INTERNAL_DATABASE_URL = "hibernate.connection.url";
    public static final String INTERNAL_DATABASE_USERNAME = "hibernate.connection.username";
    public static final String INTERNAL_DATABASE_PASSWORD = "hibernate.connection.password";
    public static final String INTERNAL_DATABASE_POOL_SIZE = "hibernate.connection.pool_size";
    public static final String INTERNAL_DATABASE_DIALECT = "hibernate.dialect";
    public static final String INTERNAL_CURRENT_SESSION_CONTEXT_CASS = "hibernate.current_session_context_class";
     public static final String INTERNAL_DATABASE_PREFIX = "internal.database.tableprefix";
    
    public static final String EXTERNAL_DATABASE_DRIVER_CLASS = "external.database.driver";
    public static final String EXTERNAL_DATABASE_URL = "external.database.connection";
    public static final String EXTERNAL_DATABASE_USERNAME = "external.database.username";
    public static final String EXTERNAL_DATABASE_PASSWORD = "external.database.password";
    public static final String EXTERNAL_DATABASE_TABLE_PREFIX = "external.database.tableprefix";
 
    public static Properties loadBaseProperties() throws MalformedURLException, IOException {
        String fileName = BASE_PROPERTIES_FILE;
        URL url = null;
        File file = new File(fileName);
        if (file.exists()) {
            url = file.toURI().toURL();
        } else {
            url = Object.class.getResource("/" + fileName);
        }

        Properties props = new Properties();
        props.load(url.openStream());
        return props;
    }

    public static Properties loadConfigurationProperties() throws MalformedURLException, IOException {
        String fileName = CONFIGURATION_PROPERTIES_FILE;
        URL url = null;
        File file = new File(fileName);
        if (file.exists()) {
            url = file.toURI().toURL();
        } else {
            url = Object.class.getResource("/" + fileName);
        }

        Properties props = new Properties();
        props.load(url.openStream());
        return props;
    }

    public static Properties loadMergedProperties() throws MalformedURLException, IOException {
        
        Properties base = loadBaseProperties();
        Properties conf = loadConfigurationProperties();
        Properties merged = new Properties();
        merged.putAll(base);
        merged.putAll(conf);

        return merged;
    }
}
